;;;; package.lisp

(defpackage #:time
  (:use #:common-lisp)
  (:export #:seconds
           #:hours-and-minutes))
