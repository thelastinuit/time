;;;; time.asd

(asdf:defsystem #:time
  :description "a simple time conversion tool"
  :author "thelastinuit <email@luisignac.io>"
  :license  "MIT"
  :version "0.0.1"
  :serial t
  :components
  ((:module "src"
    :serial t
    :components
            ((:file "package")
             (:file "time")))))

(asdf:defsystem #:time-self-specs
  :depends-on (#:fiasco)
  :serial t
  :components ((:module "spec"
                :serial t
                :components
                        ((:file "time")))))
