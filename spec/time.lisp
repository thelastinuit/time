(push *default-pathname-defaults* ql:*local-project-directories*)
(ql:quickload :fiasco)
(ql:quickload :time)

(fiasco:define-test-package #:time-specs
  (:use :time))
(in-package #:time-specs)

(deftest test-conversion-to-hours-and-minutes ()
  (is (equal (hours-and-minutes 180) '(0 3)))
  (is (equal (hours-and-minutes 4500) '(1 15))))

(deftest test-conversion-to-seconds ()
  (is (= 60 (seconds '(0 1))))
  (is (= 4500 (seconds '(1 15)))))

(deftest double-convertion ()
  (is (= 3600 (seconds (hours-and-minutes 3600))))
  (is (= 1234 (seconds (hours-and-minutes 1234)))))


(in-package :time-specs)
(run-package-tests)
(uiop:quit 0)
